<?php

//desired position
$x = 5;

//leaves positions by second
$a[0] = 1;
$a[1] = 3;
$a[2] = 1;
$a[3] = 4;
$a[4] = 2;
$a[5] = 7;
$a[6] = 5;
$a[7] = 4;

$n = 8;//leaves count

echo findEarlierTimeForJump($x, $a, $n);

function findEarlierTimeForJump($x, $a, $n) {
    $positionsWithLeafs = [];

    if ($x < 1 || $x > 1000000000 || !is_int($x)) {
        return 'Value for x must be integer greater than 1 and less than 1,000,000,000';
    }

    if ($n < 1 || $n > 1000000000 || !is_int($n)) {
        return 'Value for n must be integer greater than 1 and less than 1,000,000,000';
    }

    for($i = 0; $i < $n; $i++){

        if ($a[$i] < 1 || $a[$i] > $x || !is_int($a[$i])) {
            return 'Values for all elements of a must be integers, greater or equal than 1 and less or equal than '.$x;
        }

        $positionsWithLeafs[$a[$i]] = 1;

        if(count($positionsWithLeafs) == $x){
            return 'The frog can cross the river after '.$i.' seconds';
        }
    }

    return "There is no enough leafs in the river, so the frog can't cross it.";
}