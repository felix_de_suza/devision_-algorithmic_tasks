<?php

$a[0] = 10;
$a[1] = 2;
$a[2] = 5;
$a[3] = 1;
$a[4] = 8;
$a[5] = 20;

echo isTriangleExists($a, count($a))."\n";

$b[0] = 10;
$b[1] = 50;
$b[2] = 5;
$b[3] = 1;

echo isTriangleExists($b, count($b));

function isTriangleExists($a, $n){

    if ($n < 3){
        return 0;
    }

    $a = mergeSort($a);

    for($i = 0; $i < $n - 2; $i++){
        if($a[$i] + $a[$i+1] > $a[$i+2]){
            return 1;
        }
    }
    return 0;
}

function mergeSort($numlist){

    if(count($numlist) == 1 ){
        return $numlist;
    }

    $mid    = ceil(count($numlist) / 2);
    $left   = array_slice($numlist, 0, $mid);
    $right  = array_slice($numlist, $mid);

    $left   = mergesort($left);
    $right  = mergesort($right);

    return merge($left, $right);
}

function merge($left, $right){
    $result     = [];
    $leftIndex  = 0;
    $rightIndex = 0;

    while($leftIndex < count($left) && $rightIndex < count($right)){

        if($left[$leftIndex] > $right[$rightIndex]){
            $result[] = $right[$rightIndex];
            $rightIndex++;
        }else{
            $result[] = $left[$leftIndex];
            $leftIndex++;
        }
    }
    while($leftIndex < count($left))
    {
        $result[] = $left[$leftIndex];
        $leftIndex++;
    }
    while($rightIndex < count($right)){
        $result[] = $right[$rightIndex];
        $rightIndex++;
    }

    return $result;
}