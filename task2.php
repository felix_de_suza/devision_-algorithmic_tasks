<?php

$x = 10;
$y = 85;
$d = 30;

echo countFrogMinJumps($x,$y,$d);

function countFrogMinJumps($x, $y, $d) {

    if ($x < 1 || $y < 1 || $d < 1) {
        return 'Values for x, y and d must be greater than 1';
    }

    if ($x > 1000000000 || $y > 1000000000 || $d > 1000000000) {
        return 'Values for x, y and d must be less than 1,000,000,000';
    }

    if(!is_int($x) || !is_int($y) || !is_int($d)){
        return 'Values for x, y and d must be an integers';
    }

    //If the current frog position is equal or greater than y
    if($y - $x <= 0){
        return 0;
    }

    //If the current frog position is equal or greater than y
    if ($y - $x < $d ) {
        return 1;
    }

    if ( ($y - $x) % $d === 0) {
        $numberOfJumps = floor(($y - $x)/$d);
    } else {
        $numberOfJumps = floor(($y - $x)/$d + 1);
    }

    return $numberOfJumps;
}